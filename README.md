# [Huawei LTE dashboard](https://gitlab.com/eidoom/huawei-lte-dashboard)

## Usage

Install [PDM](https://pdm-project.org/latest/), for example,
```shell
curl -sSL https://pdm-project.org/install-pdm.py | python3 -
```
then
```shell
./setup.bash
pdm install
pdm dev # or preview
```

## Example nginx proxy server

Serve `pdm preview` without TLS on LAN.
Use a subdomain on the host (LAN nameserver must have appropriate CNAME record).

`/etc/nginx/sites-available/cell`

```nginx
server {
    listen 80;

    server_name cell.<hostname>.lan;

    location / {
        proxy_pass http://127.0.0.1:5000/;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Prefix /;
    }
}
```

## Example systemd profile

So server starts when machine starts after `sudo systemctl enable cell`.

`/etc/systemd/system/cell.service`

```systemd
[Unit]
Description=Cellular Dashboard
After=network.target

[Service]
User=<user>
Group=<user>

WorkingDirectory=/home/<user>/git/huawei-lte-dashboard 
ExecStart=/home/<user>/.local/bin/pdm preview

Type=simple
TimeoutStopSec=20
KillMode=process
Restart=on-failure

[Install]
WantedBy=multi-user.target

```
