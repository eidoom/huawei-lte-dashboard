#!/usr/bin/env python

import tomllib

from huawei_lte_api.Connection import Connection
from huawei_lte_api.Client import Client


if __name__ == "__main__":
    with open("config.toml", "rb") as f:
        cfg = tomllib.load(f)

    with Connection(
        f"http://{cfg['address']}/",
        username=cfg["username"],
        password=cfg["password"],
    ) as connection:
        client = Client(connection)
        data = client.device.signal()

    for k, v in data.items():
        print(f"{k}: {v}")
