#!/usr/bin/env bash

CFG="config.toml"
STATIC="src/huawei_lte_dashboard/static/"
HTMX="htmx.min.js"
HTMX_VERSION="1.9.10"

echo -e "${HTMX}" > "${STATIC}/.gitignore"

if [ ! -f "${STATIC}/${HTMX}" ]; then
	wget "https://unpkg.com/htmx.org@${HTMX_VERSION}/dist/${HTMX}" --output-document="${STATIC}/${HTMX}"
fi

if [ ! -f "${CFG}" ]; then
	cp ${CFG}.template ${CFG}
fi
