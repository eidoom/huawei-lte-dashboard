import tomllib, datetime, re

from flask import (
    Flask,
    Response,
    render_template,
    url_for,
    redirect,
    request,
)
from huawei_lte_api.enums.sms import StatusEnum, BoxTypeEnum

from huawei_lte_dashboard.router_api import Request


app = Flask(__name__)

app.add_template_global(StatusEnum)
app.add_template_global(BoxTypeEnum)


@app.context_processor
def inject_nav():
    return {
        "nav": [
            {"path": url_for("signal"), "name": "signal info"},
            {"path": url_for("usage"), "name": "data usage"},
            {"path": url_for("sms"), "name": "read messages"},
        ]
    }


@app.template_filter()
def datetime_format(value: datetime.datetime, format="%d %b %Y, %H:%M"):
    return value.strftime(format)


@app.template_filter()
def datetime_iso(value: datetime.datetime):
    return value.isoformat()


@app.template_filter()
def date_format(value: datetime.date, format="%d %b %Y"):
    return value.strftime(format)


@app.template_filter()
def date_iso(value: datetime.date):
    return value.isoformat()


@app.template_filter()
def timedelta_format(value: datetime.timedelta):
    return str(value)


@app.template_filter()
def is_phone_number(value: str):
    return re.fullmatch(r"\+?\d+", value) is not None


@app.template_filter()
def timedelta_p(value: datetime.timedelta):
    """
    format specification:
        https://html.spec.whatwg.org/multipage/common-microsyntaxes.html#valid-duration-string
    to be used for datetime attribute of time element:
        https://developer.mozilla.org/en-US/docs/Web/HTML/Element/time
    """
    total_seconds = int(value.total_seconds())
    total_minutes, seconds = divmod(total_seconds, 60)
    total_hours, minutes = divmod(total_minutes, 60)
    days, hours = divmod(total_hours, 24)
    return f"P{days}DT{hours}H{minutes}M{seconds}S"


with open("config.toml", "rb") as f:
    cfg = tomllib.load(f)


@app.get("/")
def index():
    return redirect(url_for("signal"))


@app.get("/signal")
def signal():
    with Request(cfg) as r:
        r.sms_metadata()

    return render_template("signal_main.html", **r.data)


@app.get("/signal_table")
def signal_table():
    """
    LTE meter levels used:
    https://poynting.tech/articles/signal-strength-measure-rsrp-rsrq-and-sinr-reference-for-lte-cheat-sheet/
    """
    with Request(cfg) as r:
        r.now()
        r.boot_time()
        r.signal_data()

    return render_template("signal_table.html", **r.data)


# TODO update sms unread counter on tick too
@app.get("/usage")
def usage():
    with Request(cfg) as r:
        r.sms_metadata()

    return render_template("usage_main.html", **r.data)


@app.get("/usage_table")
def usage_table():
    with Request(cfg) as r:
        r.now()
        r.usage_data()

    return render_template("usage_table.html", **r.data)


# TODO show the 30s countdown with some js
@app.post("/reboot")
def reboot():
    with Request(cfg) as r:
        success = r.send_reboot()

    return "", {"HX-Redirect": url_for("rebooting" if success else "error")}


@app.get("/rebooting")
def rebooting():
    with Request(cfg) as r:
        r.sms_metadata()

    return render_template("reboot.html", **r.data)


@app.get("/error")
def error():
    return render_template("message.html", message="Error")


@app.get("/sms")
def sms():
    box_type = request.args.get(
        "box_type",
        default=BoxTypeEnum.LOCAL_INBOX,
        type=lambda x: BoxTypeEnum(int(x)),
    )
    page = request.args.get("page", default=1, type=int)
    chunk = 20
    start = (page - 1) * chunk + 1

    with Request(cfg) as r:
        r.now()
        r.sms_data(box_type, chunk, page)
        r.sms_metadata()

    messages = r.data[
        {
            BoxTypeEnum.LOCAL_INBOX: "received",
            BoxTypeEnum.LOCAL_SENT: "sent",
        }[box_type]
    ]
    pages = messages // chunk + 1

    return render_template(
        "sms.html",
        **r.data,
        box_type=box_type,
        page=page,
        start=start,
        pages=pages,
    )


# TODO update unread counter
@app.post("/mark_read")
def mark_read():
    index = request.form["index"]

    with Request(cfg) as r:
        success = r.mark_sms_read(index)

    if not success:
        return redirect(url_for("error"))

    # remove the "New <Mark read>" by returning empty string
    return ""
