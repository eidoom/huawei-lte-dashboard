import datetime, re, itertools

from huawei_lte_api.Connection import Connection
from huawei_lte_api.Client import Client
from huawei_lte_api.enums.device import ControlModeEnum
from huawei_lte_api.enums.client import ResponseEnum
from huawei_lte_api.enums.sms import SortTypeEnum, BoxTypeEnum


class Request:
    def __init__(self, cfg):
        self.cfg = cfg
        self.connection = Connection(
            f"http://{cfg['address']}/",
            username=cfg["username"],
            password=cfg["password"],
        )
        self.client = Client(self.connection)
        self.data = {}

    def __enter__(self):
        return self

    def __exit__(self, *_):
        self.connection.close()

    def send_reboot(self):
        resp = self.client.device.set_control(ControlModeEnum.REBOOT)

        return resp == ResponseEnum.OK.value

    def now(self):
        ti = self.client.s_ntp.timeinfo()

        self.data |= {
            "now": datetime.datetime.fromisoformat(ti["currentlocaltime"]),
        }

    def sms_data(self, box_type=BoxTypeEnum.LOCAL_INBOX, chunk=20, page=1):
        messages = [
            *itertools.islice(
                self.client.sms.get_messages(
                    page=page,
                    box_type=box_type,
                    read_count=chunk,
                    sort_type=SortTypeEnum.DATE,
                    ascending=False,
                ),
                chunk,
            )
        ]

        for message in messages:
            message.phones = message.phone.split(";")

        # this is not the SIM's phone number!
        # number = self.client.sms.config()["Sca"]

        self.data |= {
            # "number": number,
            "messages": messages,
        }

    def mark_sms_read(self, index):
        resp = self.client.sms.set_read(index)

        return resp == "OK"

    def sms_metadata(self):
        data = self.client.sms.sms_count()

        self.data |= {
            "unread": int(data["LocalUnread"]),
            "received": int(data["LocalInbox"]),
            "sent": int(data["LocalOutbox"]),
        }

    def signal_data(self):
        def read_unit(fstr, unit):
            vstr = fstr[: -len(unit)]
            return float(vstr) if "." in vstr else int(vstr)

        info = self.client.device.information()
        signal = self.client.device.signal()

        self.data["generation"] = {"GSM": 2, "WCDMA": 3, "LTE": 4}[info["workmode"]]

        try:
            # 4G
            # ? mode: 7
            enodeb_id = signal["enodeb_id"].lstrip("0")

            try:
                enodeb_name = self.cfg["enodeb_names"][enodeb_id]
            except KeyError:
                enodeb_name = "unknown"

            signal_stats = {
                "mode": "LTE",
                "enodeb_id": enodeb_id,
                "enodeb_name": enodeb_name,
                "cell_id": signal["cell_id"],
                "band": signal["band"],
                "rssi": read_unit(signal["rssi"], "dBm"),
                "ulbw": read_unit(signal["ulbandwidth"], "MHz"),
                "dlbw": read_unit(signal["dlbandwidth"], "MHz"),
                "ulf": int(signal["lteulfreq"]) / 10,  # MHz
                "dlf": int(signal["ltedlfreq"]) / 10,  # MHz
                "rsrp": read_unit(signal["rsrp"], "dBm"),
                "sinr": read_unit(signal["sinr"], "dB"),
                "rsrq": read_unit(signal["rsrq"], "dB"),
            }
        except AttributeError:
            # 3G
            # ? mode: 2
            try:
                tx_pwr = read_unit(signal["txpower"][8:], "dBm")
            except TypeError:
                tx_pwr = 0

            signal_stats = {
                "mode": "3G",
                "cell_id": signal["cell_id"],
                "rssi": read_unit(signal["rssi"], "dBm"),
                "rscp": read_unit(signal["rscp"], "dBm"),
                "ecio": read_unit(signal["ecio"], "dB"),
                "uarfcn_dl": signal["wdlfreq"],
                "tx_pwr": tx_pwr,
            }

        self.data |= signal_stats

    def boot_time(self):
        while True:
            try:
                now = self.data["now"]
                break
            except KeyError:
                self.now()

        boot_time = self.client.device.boot_time()

        uptime = datetime.timedelta(
            **{
                k: int(v)
                for k, v in zip(
                    ("hours", "minutes", "seconds"),
                    re.fullmatch(
                        r"\[(\d+)\]:\[(\d+)\]:\[(\d+)\]", boot_time["boot_time"]
                    ).groups(),
                )
            }
        )

        self.data |= {
            "uptime": uptime,
            "last_boot": now - uptime,
        }

    def usage_data(self):
        def get_data_size(n, bit=False):
            pres = ["", "Ki", "Mi", "Gi", "Ti"]

            if bit:
                n *= 8

            pre = 0

            while n > 1024 and pre < len(pres) - 1:
                n /= 1024
                pre += 1

            return n, pres[pre] + ("b" if bit else "B")

        def get_timedelta(total_seconds):
            days, seconds = divmod(total_seconds, 86400)
            return datetime.timedelta(days=days, seconds=seconds)

        def format_data_size(quantity, unit):
            return f"{quantity:.1f} {unit}"

        while True:
            try:
                now = self.data["now"]
                break
            except KeyError:
                self.now()

        traffic = self.client.monitoring.traffic_statistics()
        month = self.client.monitoring.month_statistics()

        upload_rate, upload_rate_unit = get_data_size(
            int(traffic["CurrentUploadRate"]), bit=True
        )
        download_rate, download_rate_unit = get_data_size(
            int(traffic["CurrentDownloadRate"]), bit=True
        )
        rates = {
            "upload_rate": format_data_size(upload_rate, upload_rate_unit) + "/s",
            "download_rate": format_data_size(download_rate, download_rate_unit) + "/s",
        }

        day_usage, day_usage_unit = get_data_size(int(month["CurrentDayUsed"]))
        day_duration = get_timedelta(int(month["CurrentDayDuration"]))
        day_stats = {
            "day_usage": format_data_size(day_usage, day_usage_unit),
            "day_duration": day_duration,
        }

        month_duration = get_timedelta(int(month["MonthDuration"]))
        month_start = datetime.date(*map(int, month["MonthLastClearTime"].split("-")))
        month_upload_bytes = int(month["CurrentMonthUpload"])
        month_upload, month_upload_unit = get_data_size(month_upload_bytes)
        month_download_bytes = int(month["CurrentMonthDownload"])
        month_download, month_download_unit = get_data_size(month_download_bytes)
        month_usage, month_usage_unit = get_data_size(
            month_upload_bytes + month_download_bytes
        )
        month_stats = {
            "month_upload": format_data_size(month_upload, month_upload_unit),
            "month_download": format_data_size(month_download, month_download_unit),
            "month_usage": format_data_size(month_usage, month_usage_unit),
            "month_start": month_start,
            "month_duration": month_duration,
        }

        session_duration = get_timedelta(int(traffic["CurrentConnectTime"]))
        session_start = now - session_duration
        session_upload_bytes = int(traffic["CurrentUpload"])
        session_upload, session_upload_unit = get_data_size(session_upload_bytes)
        session_download_bytes = int(traffic["CurrentDownload"])
        session_download, session_download_unit = get_data_size(session_download_bytes)
        session_usage, session_usage_unit = get_data_size(
            session_upload_bytes + session_download_bytes
        )
        session_stats = {
            "session_upload": format_data_size(session_upload, session_upload_unit),
            "session_download": format_data_size(
                session_download, session_download_unit
            ),
            "session_usage": format_data_size(session_usage, session_usage_unit),
            "session_duration": session_duration,
            "session_start": session_start,
        }

        total_duration = get_timedelta(int(traffic["TotalConnectTime"]))
        total_start = now - total_duration
        total_upload_bytes = int(traffic["TotalUpload"])
        total_upload, total_upload_unit = get_data_size(total_upload_bytes)
        total_download_bytes = int(traffic["TotalDownload"])
        total_download, total_download_unit = get_data_size(total_download_bytes)
        total_usage, total_usage_unit = get_data_size(
            total_upload_bytes + total_download_bytes
        )

        total_stats = {
            "total_upload": format_data_size(total_upload, total_upload_unit),
            "total_download": format_data_size(total_download, total_download_unit),
            "total_usage": format_data_size(total_usage, total_usage_unit),
            "total_duration": total_duration,
            "total_start": total_start,
        }

        self.data |= rates | day_stats | month_stats | session_stats | total_stats
